package com.sachin.springboot.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.sachin.springboot.services.RemoteCallService;


public class ConsumerClientController {

	@Autowired
	private DiscoveryClient discoveryClient;
	
	@Autowired
	private LoadBalancerClient loadBalancer;
	
	@Autowired
	private RemoteCallService remoteLoadBalancer;
		
	public void getEmployee() throws RestClientException, IOException {
		//ServiceInstance serviceInstance=loadBalancer.choose("employee-producer");
		//System.out.println(serviceInstance.getUri());
		//String baseUrl=serviceInstance.getUri().toString();

		//List<ServiceInstance> instances=discoveryClient.getInstances("employee-producer");
		List<ServiceInstance> instances=discoveryClient.getInstances("EMPLOYEE-ZUUL-SERVICE");
		ServiceInstance serviceInstance=instances.get(0);
		
		String baseUrl = serviceInstance.getUri().toString();

		baseUrl = baseUrl + "/producer/employee";
		
		//baseUrl=baseUrl+"/employee";
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response=null;
		try{
		response=restTemplate.exchange(baseUrl,
				HttpMethod.GET, getHeaders(),String.class);
		}catch (Exception ex)
		{
			System.out.println(ex);
		}
		System.out.println(response.getBody());
	}
	
	public void getFeignEmployee() throws RestClientException, IOException {

		try {
			Employee emp = remoteLoadBalancer.getData();
			System.out.println(emp.getEmpId());
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}

	private static HttpEntity<?> getHeaders() throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		return new HttpEntity<>(headers);
	}
}
