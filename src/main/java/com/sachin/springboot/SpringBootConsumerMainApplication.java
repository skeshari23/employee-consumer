package com.sachin.springboot;

	import java.io.IOException;

	import org.springframework.boot.SpringApplication;
	import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
	import org.springframework.context.annotation.Bean;
	import org.springframework.web.client.RestClientException;
	import com.sachin.springboot.controllers.ConsumerClientController;

	@SpringBootApplication
	@EnableFeignClients
	public class SpringBootConsumerMainApplication {

		public static void main(String[] args) throws RestClientException, IOException {
			ApplicationContext ctx = SpringApplication.run(
					SpringBootConsumerMainApplication.class, args);
			
			ConsumerClientController consumerControllerClient=ctx.getBean(ConsumerClientController.class);
			System.out.println(consumerControllerClient);
			consumerControllerClient.getEmployee();
			
			consumerControllerClient.getFeignEmployee();
			//For Hysterix Circuit Breaker test
			//for(int i=0;i<100;i++)		
			//	consumerControllerClient.getEmployee();
			
		}
		
		@Bean
		public  ConsumerClientController  consumerControllerClient()
		{
			return  new ConsumerClientController();
		}
}
